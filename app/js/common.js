$(document).ready(function() {

	//Search for a winning combination in a row
	function inRow (arr, row, column, num, x, y) {
		let ix,
				result, 
				count; 

		result = 0;
		count = 0;

		for(ix = 0; ix < x; ix++) {
	
			(arr[row][ix] == count) ? result++ : result=1;
			if(arr[row][ix] != 0 && result == num) return arr[row][ix];
			count = arr[row][ix];
		}
		return 0;
	}

	//Search for a winning combination in a column
	function inColumn (arr, row, column, num, x, y) {
		let ix,
				result, 
				count; 

		result = 0;
		count = 0;
		
		for(ix = 0; ix < y; ix++) {
	
			(arr[ix][column] == count) ? result++ : result=1;
			if(arr[ix][column] != 0 && result == num) return arr[ix][column];
			count = arr[ix][column];
		}
		return 0;
	}

	//search for a winning combination on the left diagonal
	function inLeftDiag (arr, row, column, num, x, y) {
		let i,
				offset,
				result,
				count;

		for (offset = -x+num; offset <= y-num; offset++) {
			result = 0;
			count = 0;

			for (i = 0; i<x && i+offset<y; i++) {
			
				if(i+offset<0) continue;
				(arr[offset+i][i] == count) ? result++ : result=1;
				if(arr[offset+i][i]!=0 && result==num) return arr[offset+i][i];
				count = arr[offset+i][i];
			}
		}

		return 0;
	}

	//search for a winning combination on the right diagonal
	function inRightDiag (arr, row, column, num, x, y) {
		let i,
				offset,
				result,
				count;

		for (offset = -x+num; offset <= y-num; offset++) {
			result = 0;
			count = 0;
			
			for (i = 0; i<x && i+offset<y; i++) {
			
				if(i+offset<0) continue;
				(arr[offset+i][x-1-i] == count) ? result++ : result=1;
				if(arr[offset+i][x-1-i]!=0 && result==num) return arr[offset+i][x-1-i];
				count = arr[offset+i][x-1-i];
			}
		}
		return 0;
	}

	//Search for a winning combination on the field
	function winner (arr, row, column, num, x, y) {
		return	inRow (arr, row, column, num, x, y) ||
						inColumn (arr, row, column, num, x, y) ||
						inLeftDiag (arr, row, column, num, x, y) ||
						inRightDiag (arr, row, column, num, x, y);
	}

	//StandOFF function
	function deadHeat(arr, x, y) {
		let ix, 
				iy,
				result,
				count;

		result = 0;
		count = 0;
		
		for (ix = 0; ix < x; ix++) {
			for(iy = 0; iy < y; iy++) {
				if(arr[ix][iy] === 0) {return 0;}
			}
		}
		return 1;
	}

	var config = {};

	//Start
	$(".start").click(function () {
		let width = 360px,
				margin = "0 auto",
				num = 0,
				player = 1,
				arr = [],
				playerSym = ["", "X", "O"],
				audio = new Audio();

		//Music at the beginning of the game
		audio.src = "../sound/start.mp3";
		audio.autoplay = true;

		$(this).html("Restart");

		//Remove the table before each start
		$("table").remove();

		//Config game - choose field and line
		config.x = config.y = parseInt( $('input[name=field]:checked').val() );
		config.inRow = parseInt( $('input[name=line]:checked').val() );

		//The visual effect of an inactive player
		$(".player_1").removeClass("off");
		$(".player_2").addClass("off");

		//Generate html table
		let tbl = "<table>";

		for(let iy=0; iy<config.y; iy++){

			tbl += "<tr name='"+iy+"row'>";
						arr[iy] = [];
					
			for(let ix=0; ix<config.x; ix++){

				tbl += "<td name='"+ix+"column'></td>";
				arr[iy][ix] = 0;
			}

			tbl += "</tr>";
		}

		tbl += "</table>";

		$(".wrap-field").html(tbl);
				
		//Filling table cells - buttons
		$("td").each(function(){
			$(this).html("<button class='field'>&nbsp</button>");
				num++;
			});

		//Centering of the playing field on the basis of parameters
		if(config.x === 3) {width = 215, margin = "75px auto"}
		if(config.x === 4) {width = 290, margin = "35px auto"}
		$(".wrap-field").css({"width": width, "margin": margin});

		//Event handler when you click on the cell
		$(".field").click(function(){
			let cell,
					row,
					column,
					champion,
					standoff;

			//Sound when you press cell
			audio.src = "../sound/click.mp3";
			audio.autoplay = true;

			cell = $(this);
			row = parseInt( cell.parent().parent().attr('name') );
			column = parseInt( cell.parent().attr('name') );

			if (arr[row][column] != 0) return false;
			cell.html(playerSym[player]);  
			arr[row][column] = player;
			
			//Change the visual effect of an active and inactive player
			if(player == 1) { $(".player_1, .player_2").toggleClass("off"); }
			if(player == 2) { $(".player_1, .player_2").toggleClass("off"); }


			champion = winner(arr, row, column, config.inRow, config.x, config.y);

			if(champion) {
				audio.src = "../sound/end.mp3";
				audio.autoplay = true;
				cell.html(playerSym[player]);

				setTimeout(function() {
					$("a[href^='#callback']").trigger("click");
					$("#callback p").html("Winner of the Game");
					$("#callback h1").html("Player "+champion);
				}, 500);
			}
			//Player change  (player1 or player2)
			player = player ^ 3;

			standoff = deadHeat(arr, config.x, config.y);

			if(standoff) {
				audio.src = "../sound/end.mp3";
				audio.autoplay = true;
	
				setTimeout(function() {
					$("a[href^='#callback']").trigger("click");
					$("#callback p").html("");
					$("#callback h1").html("Standoff");
				}, 500);
			}

		});

	});


	// magnific-popup 
	$("a[href^='#callback']").click(function () {
		
	}).magnificPopup({
		type:'inline',
		mainClass: 'mfp-popUp'
	});

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	}

});
